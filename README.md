Repositório para projeto de controle de usuários e produtos utilizando:

- ASP.NET MVC 5
- Domain Driven Design (DDD)
- Entity Framework
- AutoMapper
- Injeção de dependência utilizando o Ninject
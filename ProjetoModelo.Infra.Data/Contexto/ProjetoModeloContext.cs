﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using ProjetoModelo.Domain.Entities;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;
using ProjetoModelo.Infra.Data.EntityConfig;

namespace ProjetoModelo.Infra.Data.Contexto
{
    public class ProjetoModeloContext : DbContext
    {
        public ProjetoModeloContext() : base("ProjetoModeloDDD")
        {

        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Produto> Produtos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //remover "pluralização" dos nomes das tabelas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //remover a deleção em cascata de 1 para N
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //remover a deleção em cascata de N para N
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            //Setar chave primária para elementos da tabela que possui "ID" no nome
            modelBuilder.Properties()
                .Where(p => p.Name == p.ReflectedType.Name + "Id")
                .Configure(p => p.IsKey());

            //Setar tudo que for string, como varchar e não nvarchar para que se poupe mais espaço
            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            modelBuilder.Configurations.Add(new ClienteConfiguration());

            modelBuilder.Configurations.Add(new ProdutoConfiguration());
        }

        //Checagem do valor da data cadastro no change tracker, 
        //se estiver adicionando, atribui o valor, caso contrário mantém o valor
        public override int SaveChanges()
        {

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))

            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataCadastro").IsModified = false;
                }
            }
            return base.SaveChanges();
        }
       
    }
}
